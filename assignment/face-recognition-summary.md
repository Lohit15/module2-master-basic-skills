# Face Recognition
Recognize and manipulate faces from Python or from the command line with
the world’s simplest face recognition library.
Built using dlib’s state-of-the-art face recognition
built with deep learning. The model has an accuracy of 99.38% on the
Labeled Faces in the Wild benchmark.
This also provides a simple face_recognition command line tool that lets
you do face recognition on a folder of images from the command line!
img=https://warehouse-camo.ingress.cmh1.psfhosted.org/3ab6796dd94afd8427bbe2a19b3d8f37cf888200/68747470733a2f2f636c6f75642e67697468756275736572636f6e74656e742e636f6d2f6173736574732f3839363639322f32333632353232372f34326336353336302d303235642d313165372d393465612d6231326632386362333462342e706e67
# Find and manipulate facial features in pictures
Get the locations and outlines of each person’s eyes, nose, mouth and chin.
https://warehouse-camo.ingress.cmh1.psfhosted.org/23421adfece2a539c59f2655ba724219acbc658b/68747470733a2f2f636c6f75642e67697468756275736572636f6e74656e742e636f6d2f6173736574732f3839363639322f32333632353238322f37663264373964632d303235642d313165372d383732382d6438393234353936663866612e706e67
import face_recognition
image = face_recognition.load_image_file("your_file.jpg")
face_landmarks_list = face_recognition.face_landmarks(image)
Finding facial features is super useful for lots of important stuff. But you can also use for really stupid stuff
like applying digital make-up (think ‘Meitu’):
https://warehouse-camo.ingress.cmh1.psfhosted.org/210725fb9fd2ad83b128fe7be7d965f677ec1337/68747470733a2f2f636c6f75642e67697468756275736572636f6e74656e742e636f6d2f6173736574732f3839363639322f32333632353238332f38303633383736302d303235642d313165372d383061322d3164323737396637636361622e706e67
# Identify faces in pictures
Recognize who appears in each photo.
import face_recognition
known_image = face_recognition.load_image_file("biden.jpg")
unknown_image = face_recognition.load_image_file("unknown.jpg")

biden_encoding = face_recognition.face_encodings(known_image)[0]
unknown_encoding = face_recognition.face_encodings(unknown_image)[0]

results = face_recognition.compare_faces([biden_encoding], unknown_encoding)
You can even use this library with other Python libraries to do real-time face recognition:

https://warehouse-camo.ingress.cmh1.psfhosted.org/5a78359ea27bd2ac223d7efb0f90810d77908461/68747470733a2f2f636c6f75642e67697468756275736572636f6e74656e742e636f6d2f6173736574732f3839363639322f32343433303339382f33366630653366302d313363622d313165372d383235382d3464306339636531653431392e676966
# Installing on Mac or Linux
First, make sure you have dlib already installed with Python bindings:

How to install dlib from source on macOS or Ubuntu
Then, install this module from pypi using pip3 (or pip2 for Python 2):

pip3 install face_recognition
If you are having trouble with installation, you can also try out a
pre-configured VM.
Installing on Raspberry Pi 2+
Raspberry Pi 2+ installation instructions
# Installing on Windows
While Windows isn’t officially supported, helpful users have posted instructions on how to install this library:

@masoudr’s Windows 10 installation guide (dlib + face_recognition)
# Command-Line Interface
When you install face_recognition, you get a simple command-line program
called face_recognition that you can use to recognize faces in a
photograph or folder full for photographs.
First, you need to provide a folder with one picture of each person you
already know. There should be one image file for each person with the
files named according to who is in the picture:
https://warehouse-camo.ingress.cmh1.psfhosted.org/d2f411bce6e4b41d17ce88a893f5cd14af10619f/68747470733a2f2f636c6f75642e67697468756275736572636f6e74656e742e636f6d2f6173736574732f3839363639322f32333538323436362f38333234383130652d303064662d313165372d383263662d3431353135656261373034642e706e67
Then in you simply run the command face_recognition, passing in
the folder of known people and the folder (or single image) with unknown
people and it tells you who is in each image:
$ face_recognition ./pictures_of_people_i_know/ ./unknown_pictures/

/unknown_pictures/unknown.jpg,Barack Obama
/face_recognition_test/unknown_pictures/unknown.jpg,unknown_person
There’s one line in the output for each face. The data is comma-separated
with the filename and the name of the person found.
An unknown_person is a face in the image that didn’t match anyone in
your folder of known people.