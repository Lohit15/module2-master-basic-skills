# Image Segmentation
We all are pretty aware of the endless possibilities offered by Photoshop or similar graphics editors that take a person from one image and place them into another. However, the first step of doing this is identifying where that person is in the source image and this is where Image Segmentation comes into play. There are many libraries written for Image Analysis purposes. In this article, we will be discussing in detail about scikit-image, a Python-based image processing library.
# Overview of Images in Python
Before proceeding with the technicalities of Image Segmentation, it is essential to get a little familiar with the scikit image ecosystem and how it handles images.
Importing a GrayScale Image from the skimage library
The skimage data module contains some inbuilt example data sets which are generally stored in jpeg or png format.
from skimage import data
import numpy as np
import matplotlib.pyplot as plt
image = data.binary_blobs()
plt.imshow(image, cmap='gray')
https://miro.medium.com/max/262/1*zjJS3h1Yfahkdcvov-X76Q.png
# Importing a Colored Image from the skimage library
from skimage import data
import numpy as np
import matplotlib.pyplot as plt
image = data.astronaut()
plt.imshow(image)
https://miro.medium.com/max/262/1*GzdmwcDvp8jJ2vXA_johhg.png

# Importing an image from an external source
The I/O module is used for importing the image
from skimage import data
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
image = io.imread('skimage_logo.png')
plt.imshow(image);
https://miro.medium.com/max/378/1*RS0cA3nB6Q0oHHNvyOGCRg.png

# Image segmentation
Now that we have an idea about scikit-image, let us get into details of Image Segmentation. Image Segmentation is essentially the process of partitioning a digital image into multiple segments to simplify and/or change the representation of an image into something that is more meaningful and easier to analyze.
https://miro.medium.com/max/700/1*zKnOz-YWIKtIohhYcydNEQ.png
# Basic Imports
import numpy as np
import matplotlib.pyplot as plt
import skimage.data as data
import skimage.segmentation as seg
import skimage.filters as filters
import skimage.draw as draw
import skimage.color as color
# Active contour segmentation
Active Contour segmentation also called snakes and is initialized using a user-defined contour or line, around the area of interest, and this contour then slowly contracts and is attracted or repelled from light and edges.
For our example image, let’s draw a circle around the person’s head to initialize the snake.
https://miro.medium.com/max/700/1*lErgdYIszHl7ldOhypNXyA.png
https://miro.medium.com/max/700/1*kFqeXPRL4luHygrHAflzDg.png
https://miro.medium.com/max/700/1*E86k-8aHncrcWwQv5Sov3A.png
https://miro.medium.com/max/700/1*oBqfhpKdxu5L8S2Sc9XuKQ.png
# Unsupervised segmentation
Unsupervised segmentation requires no prior knowledge. Consider an image that is so large that it is not feasible to consider all pixels simultaneously. So in such cases, Unsupervised segmentation can breakdown the image into several sub-regions, so instead of millions of pixels, you have tens to hundreds of regions. Let’s look at two such algorithms:
# SLIC( Simple Linear Iterative Clustering)
SLIC algorithm actually uses a machine-learning algorithm called K-Means under the hood. It takes in all the pixel values of the image and tries to separate them out into the given number of sub-regions.
# Felzenszwalb
This algorithm also uses a machine-learning algorithm called minimum-spanning tree clustering under the hood. Felzenszwaib doesn’t tell us the exact number of clusters that the image will be partitioned into. It’s going to run and generate as many clusters as it thinks is appropriate for that
given scale or zoom factor on the image.
# Conclusion
Image segmentation is a very important image processing step. It is an active area of research with applications ranging from computer vision to medical imagery to traffic and video surveillance. Python provides a robust library in the form of scikit-image having a large number of algorithms for image processing. It is available free of charge and free of restriction having an active community behind it. 