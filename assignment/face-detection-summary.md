Face-detection :
Face detection done with the help of the opencv and pyhton.Face detection is a computer vision technology that helps to locate/visualize human faces in digital images. This technique is a specific use case of object detection technology that deals with detecting instances of semantic objects of a certain class (such as humans, buildings or cars) in digital images and videos.

In opencv "haarcascade_frontalface_default.xml" api is used to detect the face. In opencv python you can assign name to the object and identify the objects with the different api or with the help of external file etc.

Face detection and face recognition are two different things. In detection it will only detect the face but in indentifiction of the object is done.

simple program of the face detection.

import opencv
import numpy as np

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml') # For face detection

# To capture video from webcam 
cap = cv2.VideoCapture(0)
# To use a video file as input 
# cap = cv2.VideoCapture('filename.mp4')

while True:
    # Read the frame
    ret, img = cap.read() # ret is status check and img is for frame reading
    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Detect the faces
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)
    # Draw the rectangle around each face
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
    # Display
    cv2.imshow('img', img)
    # Stop if escape key is pressed
    k = cv2.waitKey(30) & 0xff
    if k==27:
        break
# Release the VideoCapture object
cap.release()
